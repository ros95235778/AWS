

# AMI ID Deep Learning AMI Ubuntu Linux - 2.2_Aug2017 (ami-599a7721)   -我经常用的一个GPU实例，包含keras, tensorflow， maxnet, anaconda等。

# cd 到你的密钥文件所在的文件夹
# sudo chmod 400 yourkey.pem    #可能需要这步，更改密钥文件的权限

# ssh -i yourkey.pen ubuntu@123.123.123.123
# 你的用户名可能是ubuntu, ec2-user, root 等等。
# 123.123.123.123是服务器的IP地址
# 也可以使用类似这样的地址： ec2-34-208-48-45.us-west-2.compute.amazonaws.com

# nvidia-smi 查看服务器GPU信息

# sudo pip3 install wget   -安装wget库
# sudo pip3 install --upgrade keras   -更新keras
# suo pip3 install tensorflow-gpu    - 安装gpu版本 tensorflow
# sudo apt install p7zip   -安装用来解压.7z文件的库： p7zip
# p7zip -d something.7z   -解压.7z文件



import wget
import zipfile

def download(url):   
    filename = wget.download(url)
    print('\n',filename, 'downloaded!\n')
    return filename

def unzip(filename, path = None):
    '''
    这个函数是用来解压.zip格式的文件，如果不是.zip格式的压缩文件请把这个函数注释掉
    '''
    with zipfile.ZipFile(filename,"r") as zip_ref:
        if not path:
            zip_ref.extractall()
        else:
            zip_ref.extractall(path)
    print('\n', filename, 'unzipped!\n')
    
if __name__ == '__main__':
       
    url = 'https://dl.dropboxusercontent.com/s/o9b6ns44wjhq8vt/train.json.7z?dl=0'   #文件名 train.json.7z
    #url = 'https://dl.dropboxusercontent.com/s/ae2zaupt1x6lz4v/test.json.7z?dl=0'    #文件名 test.json.7z
    file_name = download(url)
    
    # 这个函数是用来解压.zip格式的文件，如果不是.zip格式的压缩文件请把这个函数注释掉
    #unzip(file_name)
    
    
    
    
    
    
    